/*
 * models.go
 *
 * Copyright 2017 Pavel Agafonov <pavel@pavel-NLS>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
package main

import "time"

type PBX struct {
    Id        int `gorm:"primary_key;AUTO_INCREMENT"`
    Name      string
    Ipaddress string `gorm:"size:20;unique"`
    SSHport   int
    Webport   int
    Status    int `gorm:"default:0;nul"`
    Lastcheck time.Time
    Title     string
    Peers     string `gorm:"default:'-'"`
    Ram       string `gorm:"default:'-'"`
    Hdd       string `gorm:"default:'-'"`
    Uptime    string `gorm:"default:'-'"`
}

type Pbxusers struct {
    Id       int    `gorm:"primary_key;AUTO_INCREMENT"`
    Username string `gorm:"type:varchar(100);unique"`
    Secret   string
}

type Auth_User struct {
    Id           int `gorm:"primary_key;AUTO_INCREMENT"`
    Password     string
    Is_superuser bool
    First_name   string
    Last_name    string
    Email        string
    Is_staff     bool
    Is_active    bool
    Username     string `gorm:"size:20;unique"`
    Sessionkey   string `gorm:"size:64;unique"`
}

type Log_record struct{
    Id           int `gorm:"primary_key;AUTO_INCREMENT"`
    Pbx          int
    Rectime      time.Time
    Title        string
}

type Cookie struct {
    Name    string
    Value   string
    Expires time.Time
}

type Peerstatus struct {
    Name      string
    Ipaddress string
    Status    bool
}

type Pbxid struct {
    Stations []PBX
    St       PBX
    Status   []Peerstatus
    Ram      []string
    Hdd      []string
    Staff    bool
    Pass     string
    Stlog    []Log_record
}

type Usercreation struct {
    Users []Auth_User
    Error string
}

type Usermodify struct {
    Users []Auth_User
    User  Auth_User
    Error string
}

type Pbxcreation struct {
    Pbxs  []PBX
    Error string
}

type PBXmodify struct {
    Pbxs  []PBX
    Pbx   PBX
    Error string
}
