package main

import (
    "fmt"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    "golang.org/x/crypto/ssh"
    "time"
)

type PBX struct {
    Id        int `gorm:"primary_key;AUTO_INCREMENT"`
    Name      string
    Ipaddress string `gorm:"size:20;unique"`
    SSHport   int
    Webport   int
    Status    int `gorm:"default:0;nul"`
    Lastcheck time.Time
    Title     string
    Peers     string `gorm:"default:'-'"`
    Ram       string `gorm:"default:'-'"`
    Hdd       string `gorm:"default:'-'"`
    Uptime    string `gorm:"default:'-'"`
}

type Pbxusers struct {
    Id       int    `gorm:"primary_key;AUTO_INCREMENT"`
    Username string `gorm:"type:varchar(100);unique"`
    Secret   string
}

type Log_record struct{
    Id           int `gorm:"primary_key;AUTO_INCREMENT"`
    Pbx          int
    Rectime      time.Time
    Title        string
}

func stopdial(conn *ssh.Client) {
    time.Sleep(54 * time.Second)
    err := conn.Close()
    if err != nil {
        return
    }

}

func pbxquery(station PBX) {
    var pbxuser Pbxusers
    var command string
    var b []byte
    var record Log_record
    command = "asterisk -rx 'sip show peers'"

    Db.Where("id = ?", 1).First(&pbxuser)
    user := pbxuser.Username
    signer := pbxuser.Secret

    config := &ssh.ClientConfig{
        User:            user,
        HostKeyCallback: ssh.InsecureIgnoreHostKey(),
        Auth: []ssh.AuthMethod{
            ssh.Password(signer),
        },
        Timeout: time.Duration(time.Second * 55),
    }

    fmt.Println("Examine station:", station.Name)
    addr := fmt.Sprintf("%s:%d", station.Ipaddress, station.SSHport)
    client, err := ssh.Dial("tcp", addr, config)
    //client.Close()
    if err != nil {
        if station.Status == 1{
            Db.Model(&station).Update("Status", 0)
            record.Pbx = station.Id
            record.Rectime = time.Now()
            record.Title = "Нет соединения со станцией"
            Db.Create(&record)
        }
    } else {
        go stopdial(client)
        session, err := client.NewSession()
        if err != nil {
            if station.Status == 1{
                Db.Model(&station).Update("Status", 0)
                record.Pbx = station.Id
                record.Rectime = time.Now()
                record.Title = "Превышен интервал ожидания"
                Db.Create(&record)
            }
        } else {
            command = "asterisk -rx 'sip show peers'"
            b, err := session.Output(command)
            if err != nil {

                if station.Status == 1{
                    Db.Model(&station).Update("Status", 0)
                    record.Pbx = station.Id
                    record.Rectime = time.Now()
                    record.Title = "Програмная ошибка станции"
                    Db.Create(&record)
                }
                return
            } else {
                station.Peers = string(b)
                station.Lastcheck = time.Now()
                if station.Status == 0{
                    station.Status = 1
                    record.Pbx = station.Id
                    record.Rectime = time.Now()
                    record.Title = "Работает штатно"
                    Db.Create(&record)
                }
            }
            session.Close()
        }
        session, _ = client.NewSession()
        if err != nil {
            fmt.Println(err)
        } else {
            //command = "egrep --color 'Mem|Cache|Swap' /proc/meminfo"
            command = "ifconfig | grep 'inet addr' | awk '{print $2}'"
            b, err = session.Output(command)
            if err != nil {
                fmt.Println(err)
            } else {
                station.Ram = string(b)
            }
            session.Close()
        }

        session, _ = client.NewSession()
        if err != nil {
            fmt.Println(err)
        } else {
            command = "df -h"
            b, err = session.Output(command)
            if err != nil {
                fmt.Println(err)
            } else {
                station.Hdd = string(b)
            }
            session.Close()
        }
        session, _ = client.NewSession()
        if err != nil {
            fmt.Println(err)
        } else {
            command = "uptime"
            b, err = session.Output(command)
            if err != nil {
                fmt.Println(err)
            } else {
                station.Uptime = string(b)
            }
            session.Close()
        }

        Db.Save(&station)
        if client != nil {
            client.Close()
        }
    }

}

func pbxmon() {
    var stations []PBX
        Db.Find(&stations)
    //i:=0
    for _, station := range stations {
        go pbxquery(station)
        //time.Sleep(1 * time.Second)
        //i++
        //if i == 10{
        //    time.Sleep(1 * time.Second)
        //    i=0
        //}
    }
    fmt.Println("Sleep for 60 second")
    time.Sleep(60 * time.Second)
}

var Db *gorm.DB
var User, Signer string

func main(){
    var pbxuser Pbxusers
    Db, _ = gorm.Open("postgres", "host=localhost user=pbxmon dbname=pbxdb sslmode=disable password=n3rjnf3")
    //Db2, _ = gorm.Open("postgres", "host=localhost user=pbxmon dbname=pbxdb sslmode=disable password=n3rjnf3")

    //Db, _ = gorm.Open("sqlite3", "database/pbx.db")
    //Db2, _ = gorm.Open("sqlite3", "database/pbx.db")

    Db.Where("id = ?", 1).First(&pbxuser)
    User = pbxuser.Username
    Signer = pbxuser.Secret
    Db.AutoMigrate(&PBX{})
    Db.AutoMigrate(&Log_record{})
    
    pbxmon()
    defer Db.Close()



}
