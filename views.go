/*
 * views.go
 *
 * Copyright 2017 Pavel Agafonov <pavel@pavel-NLS>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

package main

import "html/template"
import "net/http"
import "net/url"
import "github.com/julienschmidt/httprouter"
import "strings"
import "time"
import "fmt"
import "strconv"

func stparse(s string) []string {
    out := strings.Split(s, "\n")
    return out
}

func hddparse(s string) []string {
    out := strings.Split(s, "\n")
    out = strings.Split(out[2], " ")
    return out
}

func logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var user Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    fmt.Println(err)
    Db2.Where("sessionkey = ?", cookie.Value).First(&user)
    Db2.Model(&user).Update("sessionkey", keygen(64))
    c := http.Cookie{
        Name:   "abcmonsessionid",
        MaxAge: -5}
    http.SetCookie(w, &c)
    http.Redirect(w, r, "/", 301)
}

func monparse(s string) []Peerstatus {
    var perls, parsedls []string
    var client Peerstatus
    var clientlist []Peerstatus
    monlist := strings.Split(s, "\n")
    nop := 0
    if len(monlist) > 2 {
        for _, p := range monlist[1 : len(monlist)-2] {
            ls := strings.Split(p, " ")
            perls = nil
            parsedls = nil
            for _, j := range ls {
                if j != "" && j != "D" && j != "N" && j != "A" && j != "Yes" {
                    perls = append(perls, j)
                }
            }
            if len(perls) < 3 {
                nop = 3 - len(perls)
                for nop > 0 {
                    perls = append(perls, "OK")
                    nop = nop - 1
                }
            }
            parsedls = append(parsedls, perls[0])
            parsedls = append(parsedls, perls[1])
            parsedls = append(parsedls, perls[2])
            parsedls = append(parsedls, perls[3])

            client.Name = parsedls[0]
            client.Ipaddress = parsedls[1]
            if parsedls[3] == "OK" {
                client.Status = true
            } else {
                client.Status = false
            }

            clientlist = append(clientlist, client)

        }
    }

    return clientlist
}

func checkkey(k string) bool {
    var user Auth_User
    Db2.Where("sessionkey = ?", k).First(&user)
    if user.Is_active == true {
        return true
    } else {
        return false
    }
}

func checksu(k string) bool {
    var user Auth_User
    Db2.Where("sessionkey = ?", k).Attrs("is_superuser", false).FirstOrInit(&user)
    if user.Is_superuser == true {
        return true
    } else {
        return false
    }
}

func checkstaff(k string) bool {
    var user Auth_User
    Db2.Where("sessionkey = ?", k).Attrs("is_staff", false).FirstOrInit(&user)
    if user.Is_staff == true || user.Is_superuser == true {
        return true
    } else {
        return false
    }
}

func index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var Stations []PBX
    cookie, err := r.Cookie("abcmonsessionid")
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    if err == nil {
        if checkkey(cookie.Value) == true {
            Db2.Order("id").Find(&Stations)
            t, _ := template.ParseFiles("templates/index.html")
            t.Execute(w, Stations)
        } else {
            http.Redirect(w, r, "/auth", 301)
        }
    } else {
        http.Redirect(w, r, "/auth", 301)
    }

}

func board(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var Stations []PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    Db2.Order("id").Find(&Stations)
    t, _ := template.ParseFiles("templates/map.html")
    t.Execute(w, Stations)
}

func denied(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    t, _ := template.ParseFiles("templates/denied.html")
    t.Execute(w, nil)
}

func webcfg(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var pbxuser Pbxusers
    var Station PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.

    Db2.First(&pbxuser)
    Db2.First(&Station, id.ByName("id"))
    form := url.Values{}
    form.Add("input_user", pbxuser.Username)
    form.Add("input_pass", pbxuser.Secret)
    req, err := http.NewRequest("POST", Station.Ipaddress, strings.NewReader(form.Encode()))
    fmt.Println(err)
    http.Redirect(w, req, Station.Ipaddress, 301)
}

func auth(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var user Auth_User
    var getuser, secret string
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    if r.Method == "GET" {
        t, _ := template.ParseFiles("templates/auth.html")
        t.Execute(w, nil)
    } else {
        r.ParseForm()
        getuser = r.Form["username"][0]
        secret = r.Form["password"][0]
        Db2.Where("username = ?", getuser).First(&user)
        if user.Password == secret {
            expiration := time.Now().Add(12 * time.Hour)
            cookie := http.Cookie{Name: "abcmonsessionid", Value: user.Sessionkey, Expires: expiration}
            http.SetCookie(w, &cookie)
            http.Redirect(w, r, "/", 301)
        } else {
            http.Redirect(w, r, "/auth", 301)
        }
        http.Redirect(w, r, "/auth", 301)
    }

}

func adduser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var args []Usercreation
    var arg Usercreation
    var users []Auth_User
    var user Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.

    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            r.ParseForm()
            user.Username = r.Form["username"][0]
            user.Password = r.Form["password"][0]
            user.First_name = r.Form["firstname"][0]
            user.Last_name = r.Form["lastname"][0]
            user.Email = r.Form["email"][0]
            if len(r.Form["isactive"]) > 0 {
                user.Is_active, err = strconv.ParseBool(r.Form["isactive"][0])
            } else {
                user.Is_active = false
            }
            if len(r.Form["istech"]) > 0 {
                user.Is_staff, err = strconv.ParseBool(r.Form["istech"][0])
            } else {
                user.Is_staff = false
            }

            if len(r.Form["isadmin"]) > 0 {
                user.Is_superuser, err = strconv.ParseBool(r.Form["isadmin"][0])
            } else {
                user.Is_superuser = false
            }

            user.Sessionkey = keygen(64)
            if Db2.NewRecord(user) {
                err := Db2.Create(&user)
                fmt.Println(err)
                http.Redirect(w, r, "/admin/users/", 301)
            } else {
                arg.Error = "Пользователь с таким логином уже существует"
                Db2.Order("id").Find(&users)
                arg.Users = users
                args = append(args, arg)
                t, _ := template.ParseFiles("templates/usercreation.html")
                t.Execute(w, args)
            }
        } else {
            http.Redirect(w, r, "/auth", 301)
        }
    }
}

func userlist(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var users []Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            Db2.Order("id").Find(&users)
            t, _ := template.ParseFiles("templates/users.html")
            t.Execute(w, users)

        } else {
            http.Redirect(w, r, "/denied/", 301)
        }

    }
}

func usercreation(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var args []Usercreation
    var arg Usercreation
    var users []Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            Db2.Order("id").Find(&users)
            arg.Users = users
            args = append(args, arg)
            t, _ := template.ParseFiles("templates/usercreation.html")
            t.Execute(w, args)

        } else {
            http.Redirect(w, r, "/denied/", 301)
        }

    }
}

func usermodify(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var args []Usermodify
    var arg Usermodify
    var users []Auth_User
    var user Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            Db2.Order("id").Find(&users)
            Db2.First(&user, id.ByName("id"))
            arg.Users = users
            arg.User = user
            args = append(args, arg)
            t, _ := template.ParseFiles("templates/usermodify.html")
            t.Execute(w, args)

        } else {
            http.Redirect(w, r, "/denied/", 301)
        }
    }
}

func userapply(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var user Auth_User
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            Db2.First(&user, id.ByName("id"))
            r.ParseForm()
            user.Password = r.Form["password"][0]
            user.First_name = r.Form["firstname"][0]
            user.Last_name = r.Form["lastname"][0]
            user.Email = r.Form["email"][0]
            if len(r.Form["isactive"]) > 0 {
                user.Is_active, err = strconv.ParseBool(r.Form["isactive"][0])
            } else {
                user.Is_active = false
            }
            if len(r.Form["istech"]) > 0 {
                user.Is_staff, err = strconv.ParseBool(r.Form["istech"][0])
            } else {
                user.Is_staff = false
            }

            if len(r.Form["isadmin"]) > 0 {
                user.Is_superuser, err = strconv.ParseBool(r.Form["isadmin"][0])
            } else {
                user.Is_superuser = false
            }
            user.Sessionkey = keygen(64)
            Db2.Save(&user)
            http.Redirect(w, r, "/admin/users/", 301)
        } else {
            http.Redirect(w, r, "/denied/", 301)
        }
    }
}

func pbxid(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var Content Pbxid
    var args []Pbxid
    var rul Pbxusers
    var count int
    var logrec,templog []Log_record
    cookie, err := r.Cookie("abcmonsessionid")
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    if err == nil {
        if checkkey(cookie.Value) == true {
            Db2.Order("id").Find(&Content.Stations)
            Db2.First(&rul)
            Db2.Where("id = ?", id.ByName("id")).First(&Content.St)
            Db2.Where("pbx = ?", id.ByName("id")).Order("rectime").Find(&logrec)
            Content.Status = monparse(Content.St.Peers)
            Content.Ram = stparse(Content.St.Ram)
            Content.Hdd = hddparse(Content.St.Hdd)
            Content.Staff = checksu(cookie.Value)
            Content.Pass = rul.Secret
            reclen:= len(logrec)
            if reclen >= 7{
                count = reclen-7
            }else{
                count = 0
            }
            for i := reclen-1; i >= count; i-- {
               templog = append(templog,logrec[i])
            } 
            Content.Stlog=templog 

            args = append(args, Content)
            t, _ := template.ParseFiles("templates/pbxid.html")

            t.Execute(w, args)
        } else {
            fmt.Println(err)
            http.Redirect(w, r, "/auth", 301)
        }
    } else {
        fmt.Println(err)
    }

}

func pbxcreation(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var args []Pbxcreation
    var arg Pbxcreation
    var pbxs []PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            Db2.Order("id").Find(&pbxs)
            arg.Pbxs = pbxs
            args = append(args, arg)
            t, _ := template.ParseFiles("templates/pbxcreation.html")
            t.Execute(w, args)

        } else {
            http.Redirect(w, r, "/denied/", 301)
        }

    }
}

func addpbx(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var args []Pbxcreation
    var arg Pbxcreation
    var pbxs []PBX
    var pbx PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.

    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            r.ParseForm()
            pbx.Name = r.Form["name"][0]
            pbx.Ipaddress = r.Form["ipaddress"][0]
            pbx.SSHport, _ = strconv.Atoi(r.Form["ssh"][0])
            pbx.Webport, _ = strconv.Atoi(r.Form["web"][0])
            pbx.Status = 0
            pbx.Lastcheck = time.Now()
            pbx.Title = r.Form["title"][0]
            if Db2.NewRecord(pbx) {
                err := Db2.Create(&pbx)
                fmt.Println(err)
                http.Redirect(w, r, "/", 301)
            } else {
                arg.Error = "АТС с такими данными уже существует"
                Db2.Order("id").Find(&pbxs)
                arg.Pbxs = pbxs
                args = append(args, arg)
                t, _ := template.ParseFiles("templates/usercreation.html")
                t.Execute(w, args)
            }
        } else {
            http.Redirect(w, r, "/auth", 301)

        }
    }
}

func pbxedit(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var args []PBXmodify
    var arg PBXmodify
    var pbxs []PBX
    var pbx PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            Db2.Order("id").Find(&pbxs)
            Db2.First(&pbx, id.ByName("id"))
            arg.Pbxs = pbxs
            arg.Pbx = pbx
            args = append(args, arg)
            t, _ := template.ParseFiles("templates/pbxedit.html")
            t.Execute(w, args)

        } else {
            http.Redirect(w, r, "/denied/", 301)
        }

    }
}

func rootmodify(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var args []Pbxusers
    var root Pbxusers
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            Db2.First(&root, 1)
            args = append(args, root)
            t, _ := template.ParseFiles("templates/pbxacc.html")
            t.Execute(w, args)
        } else {
            http.Redirect(w, r, "/denied/", 301)
        }
    }
}

func rootapply(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var root Pbxusers
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checksu(cookie.Value) == true {
            r.ParseForm()
            Db2.First(&root, 1)
            root.Username = r.Form["username"][0]
            root.Secret = r.Form["password"][0]
            Db2.Save(&root)
            http.Redirect(w, r, "/admin/users/", 301)
        } else {
            http.Redirect(w, r, "/denied/", 301)
        }
    }
}

func modpbx(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var pbx PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.

    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            r.ParseForm()
            Db2.First(&pbx, id.ByName("id"))
            pbx.Name = r.Form["name"][0]
            pbx.Ipaddress = r.Form["ipaddress"][0]
            pbx.SSHport, _ = strconv.Atoi(r.Form["ssh"][0])
            pbx.Webport, _ = strconv.Atoi(r.Form["web"][0])
            pbx.Status = 0
            pbx.Lastcheck = time.Now()
            pbx.Title = r.Form["title"][0]
            Db2.Save(&pbx)
            http.Redirect(w, r, "/", 301)
        } else {
            http.Redirect(w, r, "/denied/", 301)

        }
    }
}

func delpbx(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var pbx PBX
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.

    cookie, err := r.Cookie("abcmonsessionid")
    if err == nil {
        if checkstaff(cookie.Value) == true {
            Db2.First(&pbx, id.ByName("id"))
            Db2.Delete(&pbx)
        } else {
            http.Redirect(w, r, "/denied/", 301)
        }

    } else {
        http.Redirect(w, r, "/denied/", 301)
    }
    http.Redirect(w, r, "/", 301)
}

func delpage(w http.ResponseWriter, r *http.Request, id httprouter.Params) {
    var args []PBXmodify
    var arg PBXmodify
    var pbxs []PBX
    var pbx PBX
    cookie, err := r.Cookie("abcmonsessionid")
    w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
    w.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
    w.Header().Set("Expires", "0")                                         // Proxies.
    if err == nil {
        if checkstaff(cookie.Value) == true {
            Db2.Order("id").Find(&pbxs)
            Db2.First(&pbx, id.ByName("id"))
            arg.Pbxs = pbxs
            arg.Pbx = pbx
            args = append(args, arg)
            t, _ := template.ParseFiles("templates/pbxdel.html")
            t.Execute(w, args)
        } else {
            http.Redirect(w, r, "/denied/", 301)
        }
    } else {
        http.Redirect(w, r, "/denied/", 301)
    }

}
