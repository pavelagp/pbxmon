/*
 * pmt.go
 *
 * Copyright 2017 Pavel Agafonov <pavel@pavel-NLS>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

package main

import (
    "fmt"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    "github.com/julienschmidt/httprouter"
    "net/http"
    "net/http/httputil"
    "net/url"
)

var Db, Db2 *gorm.DB
var User, Signer string


func main() {
    var pbxuser Pbxusers
    Db, _ = gorm.Open("postgres", "host=localhost user=pbxmon dbname=pbxdb sslmode=disable password=n3rjnf3")
    Db2, _ = gorm.Open("postgres", "host=localhost user=pbxmon dbname=pbxdb sslmode=disable password=n3rjnf3")

    //Db, _ = gorm.Open("sqlite3", "database/pbx.db")
    //Db2, _ = gorm.Open("sqlite3", "database/pbx.db")

    Db.Where("id = ?", 1).First(&pbxuser)
    User = pbxuser.Username
    Signer = pbxuser.Secret
    Db.AutoMigrate(&Auth_User{})
    Db.AutoMigrate(&PBX{})
    Db.AutoMigrate(&Log_record{})

    defer Db.Close()

    //go pbxmon()
    go updatekeys()
    u, _ := url.Parse("http://127.0.0.1:2222")
    
    router := httprouter.New()
    router.HandleMethodNotAllowed = false
    router.NotFound = httputil.NewSingleHostReverseProxy(u)
    router.GET("/", index)
    router.POST("/logout", logout)
    router.GET("/auth", auth)
    router.GET("/map", board)
    router.GET("/admin/users", userlist)
    router.GET("/denied", denied)
    router.GET("/wcfg/:id", webcfg)
    router.GET("/admin/adduser", usercreation)
    router.GET("/admin/rootmodify", rootmodify)
    router.POST("/admin/rootmodify", rootapply)
    router.GET("/admin/addpbx", pbxcreation)
    router.GET("/admin/delpbx/:id", delpbx)
    router.GET("/admin/delete/:id", delpage)
    router.GET("/admin/editpbx/:id", pbxedit)
    router.POST("/admin/editpbx/:id", modpbx)
    router.POST("/admin/addpbx", addpbx)
    router.GET("/admin/usermodify/:id", usermodify)
    router.POST("/admin/usermodify/:id", userapply)
    router.POST("/admin/adduser", adduser)
    router.POST("/auth", auth)
    router.GET("/pbx/:id", pbxid)
    
    http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static/"))))
    http.Handle("/", router)
    
    
    fmt.Println("Start serving on port 80")
    http.ListenAndServe(":8091", nil)
    
    
}
