/*
 * pmt.go
 *
 * Copyright 2017 Pavel Agafonov <pavel@pavel-NLS>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

package main

import (
    _ "github.com/jinzhu/gorm/dialects/sqlite"
    "math/rand"
    "strconv"
    "strings"
    "time"
)

func convert(b []byte) string {
    s := make([]string, len(b))
    for i := range b {
        s[i] = strconv.Itoa(int(b[i]))
    }
    return strings.Join(s, ",")
}


func keygen(n int) string {
    var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}

func updatekeys() {
    var users []Auth_User
    var user Auth_User
    forever := 0
    for forever == 0 {
        rand.Seed(time.Now().UnixNano())
        Db.Find(&users)
        for _, user = range users {
            user.Sessionkey = keygen(64)
            Db.Save(&user)
        }
        time.Sleep(24 * time.Hour)
    }
}
